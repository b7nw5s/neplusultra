# Ne Plus Ultra

Combined client+server UDP based file transfer application.
Based on the UDT protocol - via libudt - http://udt.sourceforge.net/

### Speed & Reliability
UDT is a reliable UDP based application level data transport protocol.  UDT is designed for extremely high speed networks and it has been used to support global data transfer of terabyte sized data sets.

```
./npu anax.******.com:/media/sdi1/test.mkv $HOME/test.mkv
Rate(MB/s)	RTT(ms)	CWnd	Packets	Loss
1.71		339.81	19	1172	0
2.61		381.29	19	1794	0
3.70		426.96	19	2541	128
3.34		389.75	19	2295	0
3.37		404.20	19	2313	0
4.36		411.25	19	2993	0
4.73		402.68	19	3246	77
2.94		395.20	19	2018	12

Rate(MB/s)	Time(s)
3.80		160.38
```

### Dependencies
Extremely minimal dependencies (statically linked against libudt.a) to ensure maximum portability and ease of use.
ldd information below:
```
ldd npu
	linux-vdso.so.1 (0x00007fffc3fc7000)
	libpthread.so.0 => /lib64/libpthread.so.0 (0x00007f64434a3000)
	libstdc++.so.6 => /usr/lib/gcc/x86_64-pc-linux-gnu/4.8.4/libstdc++.so.6 (0x00007f644319f000)
	libm.so.6 => /lib64/libm.so.6 (0x00007f6442e9e000)
	libgcc_s.so.1 => /usr/lib/gcc/x86_64-pc-linux-gnu/4.8.4/libgcc_s.so.1 (0x00007f6442c88000)
	libc.so.6 => /lib64/libc.so.6 (0x00007f64428f0000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f64436be000)
```

### Build
Build is via CMake, executable will be generated in the build/Release (or build/Debug) directory.
To build (on gnu-linux) you can run the build.sh bash script (or use CMake directly):
```
./build.sh
```

### Examples
To run in server (listen) mode:
```
npu -l
```

To transfer a remote file from the server:
```
npu 10.0.0.100:/home/username/file1.avi file1.avi
```

To transfer a local file to the server:
```
npu file2.avi 10.0.0.100:/home/username/file2.avi
```

To transfer all files in a remote directory [recursively] from the server:
```
npu 10.0.0.100:/home/username/directory1 local/path/directory1
```

To transfer all files in a local directory [recursively] to the server:
```
npu local/path/directory2 10.0.0.100:/home/username/directory2
```

To change the UDP port used (default 9000):
```
npu -l -p8000
npu -p8000 10.0.0.100:/home/username/file1.avi file1.avi
```
