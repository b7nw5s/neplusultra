//============================================================================
// Name        : neplusultra.cpp
// Author      : bcwinters
// Copyright   : BSD-3-Clause
// Description : UDP based file transfer application (server+client).
//============================================================================

#include <NpuIntent.h>
#include "NpuErrorCode.h"
#include "NpuConfig.h"
#include "NpuArgParser.h"
#include "actor/NpuActorFactory.h"
#include "actor/NpuActor.h"


int main(int argc, char* argv[]) {
	npu::NpuConfig config;
	npu::NpuIntent intent;

	npu::NpuArgParser npuArgParser;
	int result = npuArgParser.parse(argc, argv, config, intent);
	if (result == npu::SUCCESS) {
		npu::NpuActorFactory actorFactory;
		result = actorFactory.buildAndExecute(config, intent);
	}
	return result;
}
