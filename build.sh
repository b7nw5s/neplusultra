#!/bin/bash
#
# Builds in either Debug or Release (default) mode.
#
set -e

CRE_GENERATOR_TYPE="Unix Makefiles"

CRE_BUILD_TYPE="Release"
case "$1" in
  debug)
    CRE_BUILD_TYPE="Debug"
    shift
    ;;
  Debug)
    CRE_BUILD_TYPE="Debug"
    shift
    ;;
  release)
    CRE_BUILD_TYPE="Release"
    shift
    ;;
  Release)
    CRE_BUILD_TYPE="Release"
    shift
    ;;
esac
echo "-- Build type is: ${CRE_BUILD_TYPE}"
echo "-- Generator type is: ${CRE_GENERATOR_TYPE}"

# Create build directory if it does not exist.
if [ ! -d "build" ]; then
  mkdir "build"
fi
if [ ! -d "build/${CRE_BUILD_TYPE}" ]; then
  mkdir "build/${CRE_BUILD_TYPE}"
fi
# Change to build directory.
if [ -d "build/${CRE_BUILD_TYPE}" ]; then
  cd "build/${CRE_BUILD_TYPE}"
else
  echo "Build directory build/${CRE_BUILD_TYPE} does not exist."
  exit 1
fi

# Generate arguments to pass to cmake.
CRE_CMAKE_ARGS="-DCMAKE_BUILD_TYPE=${CRE_BUILD_TYPE} -G \"${CRE_GENERATOR_TYPE}\""
# Call cmake to generate the build files.
cmake "${CRE_CMAKE_ARGS}" ../../

# Call make passing through the remaining command line arguments.
make "$@"
