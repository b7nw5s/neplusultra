#include "catch.hpp"

#include <memory>

#include "actor/NpuActorFactory.h"
#include "actor/NpuActor.h"
#include "NpuConfig.h"

namespace npu {

	SCENARIO("actors can be created") {

		GIVEN("an actor factory and configuration") {
			NpuActorFactory actorFactory;
			NpuConfig config;

			WHEN("intent is INTENT_LISTEN and the actor is created") {
				std::unique_ptr<NpuActor> actor(actorFactory.build(config, INTENT_LISTEN));

				THEN("ServerActor is created") {
					REQUIRE(actor->toString() == "ServerActor");
				}
			}

			WHEN("intent is INTENT_GET and the actor is created") {
				std::unique_ptr<NpuActor> actor(actorFactory.build(config, INTENT_GET));

				THEN("GetActor is created") {
					REQUIRE(actor->toString() == "GetActor");
				}
			}

			WHEN("intent is INTENT_PUT and the actor is created") {
				std::unique_ptr<NpuActor> actor(actorFactory.build(config, INTENT_PUT));

				THEN("PutActor is created") {
					REQUIRE(actor->toString() == "PutActor");
				}
			}
		}
	}
}
