#ifndef NPUINTENT_H_
#define NPUINTENT_H_

namespace npu {

	enum NpuIntent {
		INTENT_UNKNOWN = 0,
		INTENT_LISTEN,
		INTENT_GET,
		INTENT_PUT
	};
}

#endif /* NPUINTENT_H_ */
