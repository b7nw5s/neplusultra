#ifndef NPUCONFIG_H_
#define NPUCONFIG_H_

#include <string>

namespace npu {
namespace config {
	enum { DEFAULT_PORT = 9000 };
}

	class NpuConfig {
	public:
		NpuConfig();

		NpuConfig(const std::string& remoteHost,
				const int port,
				const std::string& sourcePath,
				const std::string& destinationPath);

		NpuConfig(const NpuConfig& src);

		~NpuConfig();


		NpuConfig& operator= (const NpuConfig& rhs);


		const std::string& remoteHost() const;

		void setRemoteHost(const std::string& remoteHost);

		const int port() const;

		void setPort(const int port);

		const std::string& sourcePath() const;

		void setSourcePath(const std::string& sourcePath);

		const std::string& destinationPath() const;

		void setDestinationPath(const std::string& destinationPath);

	private:
		std::string remoteHost_;
		std::string sourcePath_;
		std::string destinationPath_;
		int port_;
	};


	inline const std::string& NpuConfig::remoteHost() const {
		return remoteHost_;
	}

	inline void NpuConfig::setRemoteHost(const std::string& remoteHost) {
		remoteHost_ = remoteHost;
	}

	inline const int NpuConfig::port() const {
		return port_;
	}

	inline void NpuConfig::setPort(const int port) {
		port_ = port;
	}

	inline const std::string& NpuConfig::sourcePath() const {
		return sourcePath_;
	}

	inline void NpuConfig::setSourcePath(const std::string& sourcePath) {
		sourcePath_ = sourcePath;
	}

	inline const std::string& NpuConfig::destinationPath() const {
		return destinationPath_;
	}

	inline void NpuConfig::setDestinationPath(const std::string& destinationPath) {
		destinationPath_ = destinationPath;
	}
}

#endif /* NPUCONFIG_H_ */
