#ifndef NPUSOCKET_H_
#define NPUSOCKET_H_

#include <string>
#include <stdint.h>
#include <udt4/udt.h>

#include "util/Runnable.h"
#include "NpuCommandId.h"

namespace npu {
	class NpuMonitor;

	class NpuSocket {
	public:
		NpuSocket(const UDTSOCKET& udtSocket);

		~NpuSocket();


		/**
		 * Sends a command message.
		 *
		 * @param command command type
		 * @param length length parameter
		 * @param content content parameter
		 */
		void sendCommand(const uint8_t& command,
				const int64_t& length,
				const std::string& content) const;

		/**
		 * Sends a command message (without a content parameter).
		 *
		 * @param command command type
		 * @param length length parameter
		 */
		void sendCommand(const uint8_t& command,
				const int64_t& length) const;

		/**
		 * Sends a command message (without any parameters).
		 *
		 * @param command command type
		 */
		void sendCommand(const uint8_t& command) const;

		/**
		 * Receives a command message.
		 *
		 * @param length length parameter (output)
		 * @param content content parameter (output)
		 * @return command command type
		 */
		const uint8_t receiveCommand(int64_t& length, std::string& content) const;

		/**
		 * Receives a command message (ignoring the content parameter).
		 *
		 * @param length length parameter (output)
		 * @return command command type
		 */
		const uint8_t receiveCommand(int64_t& length) const;

		/**
		 * Sends a file to the server.
		 *
		 * @param path local file path
		 * @param length file length (in bytes)
		 * @return {@code true} if succeeded, otherwise {@code false} if failed
		 */
		const bool sendFile(const std::string& path, const int64_t& length) const;

		/**
		 * Receives a file from the server.
		 *
		 * @param path local file path
		 * @param length file length (in bytes)
		 * @return {@code true} if succeeded, otherwise {@code false} if failed
		 */
		const bool receiveFile(const std::string& path, const int64_t& length) const;


		/**
		 * Begins (forks off) the monitoring thread.
		 */
		void beginMonitor();

		/**
		 * End the monitoring thread.
		 */
		void endMonitor();


	private:
		/**
		 * Sends a fixed length of bytes via the UDT socket.
		 *
		 * @param data data buffer
		 * @param length bytes to send from data buffer
		 */
		const int sendBytes(const char* const& data, const int length) const;

		/**
		 * Receives a fixed length of bytes via the UDT socket.
		 *
		 * @param data data buffer
		 * @param length bytes to receive into data buffer
		 */
		const int receiveBytes(char* const& data, const int length) const;


		const UDTSOCKET udtSocket_;
		NpuMonitor* monitor_;
	};
}

#endif /* NPUSOCKET_H_ */
