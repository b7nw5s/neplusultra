#ifndef NPUMONITOR_H_
#define NPUMONITOR_H_

#include <udt4/udt.h>

namespace npu {

	class NpuMonitor {
	public:
		NpuMonitor(const UDTSOCKET& udtSocket);

		NpuMonitor(const UDTSOCKET& udtSocket,
				const unsigned int period,
				const unsigned precision);

		~NpuMonitor();

		/**
		 * Monitoring loop.
		 */
		void monitor();

		/**
		 * Interrupts the monitoring loop.
		 */
		void interrupt();

	private:
		const UDTSOCKET udtSocket_;
		const unsigned int period_;
		const unsigned int precision_;
		volatile bool enabled_;
	};
}

#endif /* NPUMONITOR_H_ */
