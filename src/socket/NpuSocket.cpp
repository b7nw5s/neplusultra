#include "NpuSocket.h"

#include <cstring>
#include <memory>
#include <sstream>
#include <iostream>

#include "util/FileSystem.h"
#include "util/Runnable.h"

#include "NpuMonitor.h"

namespace npu {

	NpuSocket::NpuSocket(const UDTSOCKET& udtSocket) :
			udtSocket_(udtSocket), monitor_(NULL) {
	}

	NpuSocket::~NpuSocket() {
		endMonitor();
		UDT::close(udtSocket_);
	}


	void NpuSocket::sendCommand(const uint8_t& command) const {
		sendCommand(command, 0, std::string());
	}

	void NpuSocket::sendCommand(const uint8_t& command, const int64_t& length) const {
		sendCommand(command, length, std::string());
	}

	void NpuSocket::sendCommand(const uint8_t& command,
			const int64_t& length,
			const std::string& content) const {

		const int32_t headerLength = sizeof(int32_t);
		const int32_t contentLength = std::max(0, static_cast<int32_t>(content.length()));
		const int32_t bodyLength =
				// command type
				sizeof(uint8_t)
				// length parameter
				+ sizeof(int64_t)
				// content parameter
				+ (sizeof(char) * contentLength);

		const int32_t dataLength = (headerLength + bodyLength);
		char* const data = new char[dataLength];
		memset(data, 0, dataLength);

		int32_t dataOffset = 0;
		// First 4 bytes is the body length of the command.
		*reinterpret_cast<int32_t* const>(data) = bodyLength;
		dataOffset += sizeof(int32_t);

		// Next byte is the command type.
		*reinterpret_cast<uint8_t* const>(data + dataOffset) = static_cast<uint8_t>(command);
		dataOffset += sizeof(uint8_t);

		// Next 8 bytes is the length parameter.
		*reinterpret_cast<int64_t* const>(data + dataOffset) = length;
		dataOffset += sizeof(int64_t);

		// Remaining bytes are the content parameter.
		for (int32_t i = 0; i < contentLength; ++i) {
			data[dataOffset + i] = content[i];
		}
		if (sendBytes(data, dataLength) < dataLength) {
			std::cout << "failed to send command" << std::endl;
		}
		delete[] data;
	}

	const uint8_t NpuSocket::receiveCommand(int64_t& length) const {
		std::string content;
		return receiveCommand(length, content);
	}

	const uint8_t NpuSocket::receiveCommand(int64_t& length, std::string& content) const {
		uint8_t command = CMD_ERROR;
		// Receive command header (length in bytes of body of the command).
		const int32_t headerLength = sizeof(int32_t);
		char* const header = new char[headerLength];
		if (receiveBytes(header, headerLength) < headerLength) {
			std::cout << "failed to receive command header" << std::endl;
		} else {
			const int32_t bodyLength = *reinterpret_cast<int32_t* const>(header);
			if (bodyLength > 0) {
				char* const body = new char[bodyLength];
				if (receiveBytes(body, bodyLength) < bodyLength) {
					std::cout << "failed to receive command" << std::endl;
				} else {
					// First byte is the command type.
					command = *reinterpret_cast<uint8_t* const>(body);
					int32_t dataOffset = sizeof(uint8_t);

					// Next 8 bytes is the length parameter.
					length = *reinterpret_cast<int64_t* const>(body + dataOffset);
					dataOffset += sizeof(int64_t);

					// Remaining bytes are the content parameter.
					std::stringstream contentStream;
					for (int32_t i = dataOffset; i < bodyLength; ++i) {
						contentStream << body[i];
					}
					content = contentStream.str();
				}
				delete[] body;
			}
		}
		delete[] header;
		return command;
	}

	const int NpuSocket::sendBytes(const char* const& data, const int length) const {
		int ssize = 0;
		int ss;
		while (ssize < length) {
			if (UDT::ERROR == (ss = UDT::send(udtSocket_, data + ssize, length - ssize, 0))) {
				std::cout << "send failed:" << UDT::getlasterror().getErrorMessage() << std::endl;
				break;
			}
			ssize += ss;
		}
		return ssize;
	}

	const int NpuSocket::receiveBytes(char* const& data, const int length) const {
		int rsize = 0;
		int rs;
		while (rsize < length) {
			int receiveSize;
			int varSize = sizeof(int);
			UDT::getsockopt(udtSocket_, 0, UDT_RCVDATA, &receiveSize, &varSize);
			if (UDT::ERROR == (rs = UDT::recv(udtSocket_, data + rsize, length - rsize, 0))) {
				std::cout << "receive failed:" << UDT::getlasterror().getErrorMessage() << std::endl;
				break;
			}
			rsize += rs;
		}
		return rsize;
	}

	const bool NpuSocket::sendFile(const std::string& path, const int64_t& length) const {
		if (length > 0 && !util::fs::isDirectory(path)) {
			std::fstream fileInputStream(path, std::ios::in | std::ios::binary);
			if (fileInputStream.is_open()) {
				int64_t offset = 0;

				if (UDT::ERROR == UDT::sendfile(udtSocket_, fileInputStream, offset, length)) {
					std::cout << "sendfile failed: " << UDT::getlasterror().getErrorMessage();
					return false;
				}
			}
		}
		return true;
	}

	const bool NpuSocket::receiveFile(const std::string& path, const int64_t& length) const {
		if (length > 0 && !util::fs::isDirectory(path)) {
			// Ensure output directory tree exists.
			util::fs::makeDirs(path);

			std::fstream fileOutputStream(path, std::ios::out | std::ios::binary | std::ios::trunc);
			if (fileOutputStream.is_open()) {
				int64_t offset = 0;

				if (UDT::ERROR == UDT::recvfile(udtSocket_, fileOutputStream, offset, length)) {
					std::cout << "recvfile failed: " << UDT::getlasterror().getErrorMessage();
					return false;
				}
			}
		}
		return true;
	}


	RUNNABLE(runMonitor) {
		std::unique_ptr<NpuMonitor> monitor(static_cast<NpuMonitor*>(argv));
		if (monitor.get()) {
			monitor->monitor();
		}
		RUNNABLE_RETURN();
	}

	void NpuSocket::beginMonitor() {
		if (!monitor_) {
			monitor_ = new NpuMonitor(udtSocket_);
			THREAD thread;
			FORK_THREAD(thread, runMonitor, monitor_);
		}
	}

	void NpuSocket::endMonitor() {
		if (monitor_) {
			monitor_->interrupt();
			monitor_ = NULL;
		}
	}
}
