#ifndef NPUCOMMAND_H_
#define NPUCOMMAND_H_

namespace npu {

	enum NpuCommandId {
		CMD_ERROR = 0,

		// client -> server
		CMD_GET,
		CMD_PUT,
		CMD_CLOSE,

		// server -> client
		CMD_DIRECTORY,
		CMD_SEND_FILE,
		CMD_RECEIVE_FILE
	};
}

#endif /* NPUCOMMAND_H_ */
