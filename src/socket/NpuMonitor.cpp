#include "NpuMonitor.h"

#include <string>
#include <sstream>
#include <iostream>

#include "util/Sleep.h"

namespace npu {

	enum { BITS_PER_BYTE = 8 };

	enum { DEFAULT_PERIOD = 1 };
	enum { DEFAULT_PRECISION = 2 };


	NpuMonitor::NpuMonitor(const UDTSOCKET& udtSocket) :
			udtSocket_(udtSocket), period_(DEFAULT_PERIOD),
			precision_(DEFAULT_PRECISION), enabled_(false) {
	}

	NpuMonitor::NpuMonitor(const UDTSOCKET& udtSocket,
			const unsigned int period, const unsigned precision) :
			udtSocket_(udtSocket), period_(period),
			precision_(precision), enabled_(false) {
	}

	NpuMonitor::~NpuMonitor() {
	}

	void NpuMonitor::monitor() {
		enabled_ = true;

		UDT::TRACEINFO stats;

		uint32_t count = 0u;
		for (;;) {
			SLEEP(period_);
			if (!enabled_) {
				break;
			}
			if (UDT::ERROR == UDT::perfmon(udtSocket_, &stats)) {
				// Break on error and let the thread terminate.
				break;
			}
			const double mbpsRate = (stats.mbpsSendRate + stats.mbpsRecvRate);
			const double rate = (mbpsRate / static_cast<double>(BITS_PER_BYTE));
			const int totalPackets = (stats.pktSent + stats.pktRecv);
			const int totalLoss = (stats.pktSndLoss + stats.pktRcvLoss);

			std::stringstream strStats;
			strStats.setf(std::ios::fixed, std::ios::floatfield);
			strStats.precision(precision_);

			if (count == 0) {
				strStats << "Rate(MB/s)\tPackets\tLoss\tCWnd\tRTT(ms)" << std::endl;
				++count;
			}
			strStats << rate << "\t\t"
					<< totalPackets << "\t"
					<< totalLoss << "\t"
					<< stats.pktCongestionWindow << "\t"
					<< stats.msRTT << "\t";

			std::cout << strStats.str() << std::endl;
		}
	}

	void NpuMonitor::interrupt() {
		enabled_ = false;
	}
}
