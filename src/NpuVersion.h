#ifndef NPUVERSION_H_
#define NPUVERSION_H_

namespace npu {
namespace config {

	enum { VERSION_MAJOR = 1 };
	enum { VERSION_MINOR = 0 };
	enum { VERSION_PATCH = 0 };
}}

#endif /* NPUVERSION_H_ */
