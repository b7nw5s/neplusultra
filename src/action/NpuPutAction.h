#ifndef NPUPUTACTION_H_
#define NPUPUTACTION_H_

#include <string>

#include "NpuSocketAction.h"

namespace npu {

	/**
	 * Puts a file (or recursively a directory) onto the server.
	 */
	class NpuPutAction : public NpuSocketAction {
	public:
		/**
		 * Puts a file (or recursively a directory) onto the server.
		 *
		 * @param socket socket
		 * @param sourcePath local file path
		 * @param destinationPath remote file path
		 */
		NpuPutAction(NpuSocket& socket,
				const std::string& sourcePath,
				const std::string& destinationPath);

		~NpuPutAction();


		int execute();


	private:
		/**
		 * Puts a file (or recursively a directory) onto the server.
		 *
		 * @param sourcePath local file path
		 * @param destinationPath remote file path
		 * @return error code, or 0 for success
		 */
		int put(const std::string& sourcePath,
				 const std::string& destinationPath);

		/**
		 * Puts files recursively from a directory onto the server.
		 *
		 * @param sourceDirPath local directory path
		 * @param destinationDirPath remote directory path
		 * @return error code, or 0 for success
		 */
		int putDirectory(const std::string& sourceDirPath,
				const std::string& destinationDirPath);


		const std::string sourcePath_;
		const std::string destinationPath_;
	};
}

#endif /* NPUPUTACTION_H_ */
