#include "NpuPutAction.h"

#include <iostream>

#include "util/FileSystem.h"

#include "socket/NpuSocket.h"
#include "NpuErrorCode.h"

namespace npu {

	NpuPutAction::NpuPutAction(NpuSocket& socket,
			const std::string& sourcePath,
			const std::string& destinationPath) :
					NpuSocketAction(socket),
					sourcePath_(sourcePath),
					destinationPath_(destinationPath) {
	}

	NpuPutAction::~NpuPutAction() {
	}

	int NpuPutAction::execute() {
		return put(sourcePath_, destinationPath_);
	}

	int NpuPutAction::put(const std::string& sourcePath,
			const std::string& destinationPath) {
		int64_t length = 0;

		const std::string completeDestinationPath(util::fs::appendBasename(
				destinationPath, util::fs::basename(sourcePath)));

		int result = SUCCESS;
		length = util::fs::fileLength(sourcePath);
		if (length > 0) {
			socket().sendCommand(CMD_PUT, length, completeDestinationPath);

			switch (socket().receiveCommand(length)) {
			case CMD_SEND_FILE:
				std::cout << "Sending " << util::fs::basename(sourcePath)
						<< " (" << (length / MEGABYTE) << " MB)" << std::endl;
				beginMonitor();
				socket().sendFile(sourcePath, length);
				endMonitor(length);
				break;
			default:
				result = ERR_UNKNOWN_SERVER_RESPONSE;
				break;
			}
		} else if (util::fs::isDirectory(sourcePath)) {
			result = putDirectory(sourcePath, completeDestinationPath);
		} else {
			std::cout << "local file does not exist: " << sourcePath << std::endl;
			result = ERR_SOURCE_PATH_INVALID;
		}
		return result;
	}

	int NpuPutAction::putDirectory(const std::string& sourceDirPath,
			const std::string& destinationDirPath) {
		int result = SUCCESS;
		std::vector<std::string> children;
		util::fs::list(sourceDirPath, children);
		for (std::vector<std::string>::iterator iter = children.begin();
				iter != children.end(); ++iter) {
			const std::string& child(*iter);
			if (!child.empty()) {
				const std::string childSourcePath(util::fs::appendToPath(sourceDirPath, child));
				const std::string childDestinationPath(util::fs::appendToPath(destinationDirPath, child));
				result = put(childSourcePath, childDestinationPath);
				if (result != SUCCESS) {
					break;
				}
			}
		}
		return result;
	}
}
