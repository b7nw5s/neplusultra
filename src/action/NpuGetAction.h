#ifndef NPUGETACTION_H_
#define NPUGETACTION_H_

#include <string>
#include <vector>

#include "NpuSocketAction.h"

namespace npu {

	/**
	 * Gets a file (or recursively a directory) from the server.
	 */
	class NpuGetAction : public NpuSocketAction {
	public:
		/**
		 * Gets a file (or recursively a directory) from the server.
		 *
		 * @param socket socket
		 * @param sourcePath remote file path
		 * @param destinationPath local file path
		 */
		NpuGetAction(NpuSocket& socket,
				const std::string& sourcePath,
				const std::string& destinationPath);

		~NpuGetAction();


		int execute();


	private:
		/**
		 * Gets a file (or recursively a directory) from the server.
		 *
		 * @param sourcePath remote file path
		 * @param destinationPath local file path
		 * @return error code, or 0 for success
		 */
		int get(const std::string& sourcePath,
				const std::string& destinationPath);

		/**
		 * Gets files recursively in a directory from the server.
		 *
		 * @param sourceDirPath remote directory path
		 * @param children child file paths of the remote directory
		 * @param destinationDirPath local directory path
		 * @return error code, or 0 for success
		 */
		int getDirectory(const std::string& sourceDirPath,
				const std::vector<std::string>& children,
				const std::string& destinationDirPath);

		/**
		 * Splits a string on each newline \n character encountered.
		 *
		 * @param input input string to split
		 * @param result resulting tokens
		 */
		void splitOnNewline(const std::string& input,
				std::vector<std::string>& result) const;


		const std::string sourcePath_;
		const std::string destinationPath_;
	};
}

#endif /* NPUGETACTION_H_ */
