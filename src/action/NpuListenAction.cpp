#include "NpuListenAction.h"

#include <sstream>
#include <iostream>

#include "util/FileSystem.h"

#include "socket/NpuSocket.h"
#include "NpuErrorCode.h"

namespace npu {

	NpuListenAction::NpuListenAction(NpuSocket& socket) :
			NpuSocketAction(socket) {
	}

	NpuListenAction::~NpuListenAction() {
	}

	int NpuListenAction::execute() {
		// Receive initial command message.
		int64_t length = 0;
		std::string content;

		int result = SUCCESS;
		bool terminated = false;
		while (!terminated) {
			switch (socket().receiveCommand(length, content)) {
			case CMD_GET:
				handleGet(content);
				break;
			case CMD_PUT:
				handlePut(length, content);
				break;
			case CMD_CLOSE:
				terminated = true;
				break;
			case CMD_ERROR:
			default:
				result = ERR_UNKNOWN_CLIENT_REQUEST;
				terminated = true;
				break;
			}
		}
		return result;
	}

	void NpuListenAction::handleGet(const std::string& path) {
		const uint64_t length = util::fs::fileLength(path);
		if (length > 0) {
			// Send file to client.
			socket().sendCommand(CMD_RECEIVE_FILE, length);
			if (socket().sendFile(path, length)) {
				std::cout << "sent: " << path << " (" << (length / MEGABYTE) << " MB)" << std::endl;
			}
		} else {
			// Send list of directory contents to client.
			std::string entries;
			const int64_t entriesLength = ls(path, entries);
			socket().sendCommand(CMD_DIRECTORY, entriesLength, entries);
		}
	}

	void NpuListenAction::handlePut(const int64_t& length, const std::string& path) {
		if (length > 0) {
			// Receive file from client.
			socket().sendCommand(CMD_SEND_FILE, length);
			if (socket().receiveFile(path, length)) {
				std::cout << "received: " << path << " (" << (length / MEGABYTE) << " MB)" << std::endl;
			}
		}
	}

	const int NpuListenAction::ls(const std::string& path, std::string& contents) const {
		static const char delimiter = '\n';

		std::stringstream str;
		std::vector<std::string> children;
		const int length = util::fs::list(path, children);
		for (std::vector<std::string>::iterator iter = children.begin();
				iter != children.end(); ++iter) {
			str << *iter << delimiter;
		}
		contents = str.str();
		return length;
	}
}
