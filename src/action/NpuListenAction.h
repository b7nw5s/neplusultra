#ifndef NPULISTENACTION_H_
#define NPULISTENACTION_H_

#include <string>

#include "NpuSocketAction.h"

namespace npu {

	/**
	 * Listens for and processes commands from a client.
	 */
	class NpuListenAction : public NpuSocketAction {
	public:
		/**
		 * Listens for and processes commands from a client.
		 *
		 * @param socket socket
		 */
		NpuListenAction(NpuSocket& socket);

		~NpuListenAction();


		int execute();


	private:
		/**
		 * Handles a get request.
		 *
		 * @param path file path
		 */
		void handleGet(const std::string& path);

		/**
		 * Handles a put request.
		 *
		 * @param length file length
		 * @param path file path
		 */
		void handlePut(const int64_t& length, const std::string& path);

		/**
		 * Lists the contents of a directory.
		 *
		 * @param path directory path
		 * @param contents directory contents (output)
		 * @return number of entries in the directory contents
		 */
		const int ls(const std::string& path, std::string& contents) const;
	};
}

#endif /* NPULISTENACTION_H_ */
