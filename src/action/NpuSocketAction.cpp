#include "NpuSocketAction.h"

#include <string>
#include <sstream>
#include <iostream>

#include "util/Now.h"

#include "socket/NpuSocket.h"

namespace npu {

	NpuSocketAction::NpuSocketAction(NpuSocket& socket) :
			socket_(socket), monitorStartTime_(0) {
	}

	NpuSocketAction::~NpuSocketAction() {
	}

	const NpuSocket& NpuSocketAction::socket() const {
		return socket_;
	}

	NpuSocket& NpuSocketAction::socket() {
		return socket_;
	}


	void NpuSocketAction::beginMonitor() {
		socket().beginMonitor();
		monitorStartTime_ = util::time::now();
	}

	void NpuSocketAction::endMonitor(const int64_t& dataLength) {
		socket().endMonitor();
		printStats(dataLength, util::time::now() - monitorStartTime_);
	}

	void NpuSocketAction::printStats(const int64_t& dataLength, const int64_t& time) const {
		enum { DECIMAL_PRECISION = 2 };

		if (dataLength > 0 && time > 0) {
			const double seconds = (static_cast<double>(time) / 1000.0);
			const double rate = (static_cast<double>(dataLength)
					/ static_cast<double>(MEGABYTE)
					/ seconds);

			std::stringstream strStats;
			strStats.setf(std::ios::fixed, std::ios::floatfield);
			strStats.precision(DECIMAL_PRECISION);

			strStats << "Rate(MB/s)\tTime(s)" << std::endl;
			strStats << rate << "\t\t" << seconds;

			std::cout << strStats.str() << std::endl << std::endl;
		}
	}
}
