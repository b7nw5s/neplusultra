#include "NpuGetAction.h"

#include <iostream>

#include "util/FileSystem.h"

#include "socket/NpuSocket.h"
#include "NpuErrorCode.h"

namespace npu {

	NpuGetAction::NpuGetAction(NpuSocket& socket,
			const std::string& sourcePath,
			const std::string& destinationPath) :
					NpuSocketAction(socket),
					sourcePath_(sourcePath),
					destinationPath_(destinationPath) {
	}

	NpuGetAction::~NpuGetAction() {
	}

	int NpuGetAction::execute() {
		return get(sourcePath_, destinationPath_);
	}

	int NpuGetAction::get(const std::string& sourcePath, const std::string& destinationPath) {
		int64_t length = 0;
		std::string content;
		std::vector<std::string> children;

		const std::string completeDestinationPath(util::fs::appendBasename(
				destinationPath, util::fs::basename(sourcePath)));

		int result = SUCCESS;
		socket().sendCommand(CMD_GET, 0, sourcePath);

		switch (socket().receiveCommand(length, content)) {
		case CMD_RECEIVE_FILE:
			if (length > 0) {
				std::cout << "Receiving " << util::fs::basename(sourcePath)
						<< " (" << (length / MEGABYTE) << " MB)" << std::endl;
				beginMonitor();
				socket().receiveFile(completeDestinationPath, length);
				endMonitor(length);
			} else {
				std::cout << "remote file does not exist: " << sourcePath_ << std::endl;
			}
			result = SUCCESS;
			break;

		case CMD_DIRECTORY:
			if (length > 0) {
				splitOnNewline(content, children);
				result = getDirectory(sourcePath, children, completeDestinationPath);
			}
			break;

		default:
			std::cout << "unknown command received from server" << std::endl;
			result = ERR_UNKNOWN_SERVER_RESPONSE;
			break;
		}
		return result;
	}

	int NpuGetAction::getDirectory(const std::string& sourceDirPath,
			const std::vector<std::string>& children,
			const std::string& destinationDirPath) {

		int result = SUCCESS;
		for (std::vector<std::string>::const_iterator iter = children.begin();
				iter != children.end(); ++iter) {
			const std::string& child(*iter);
			if (!child.empty()) {
				const std::string childSourcePath(util::fs::appendToPath(sourceDirPath, child));
				const std::string childDestinationPath(util::fs::appendToPath(destinationDirPath, child));
				result = get(childSourcePath, childDestinationPath);
				if (result != SUCCESS) {
					break;
				}
			}
		}
		return result;
	}

	void NpuGetAction::splitOnNewline(const std::string& input,
			std::vector<std::string>& result) const {
		util::fs::splitBy(input, '\n', result);
	}
}
