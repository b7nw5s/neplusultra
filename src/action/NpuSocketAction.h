#ifndef NPUSOCKETACTION_H_
#define NPUSOCKETACTION_H_

#include <stdint.h>

namespace npu {
	class NpuSocket;

	const int MEGABYTE = 1048576;


	class NpuSocketAction {
	public:
		NpuSocketAction(NpuSocket& npuSocket);

		virtual ~NpuSocketAction();

		/**
		 * Executes the action.
		 *
		 * @return error code, or 0 on success
		 */
		virtual int execute() = 0;


	protected:
		const NpuSocket& socket() const;

		NpuSocket& socket();

		/**
		 * Begins monitoring.
		 */
		void beginMonitor();

		/**
		 * Ends monitoring (and prints final monitoring statistics).
		 *
		 * @param dataLength length of data transferred (in bytes)
		 */
		void endMonitor(const int64_t& dataLength);


	private:
		/**
		 * Print final statistics about transfer rate and time.
		 *
		 * @param length length of file transferred (in bytes)
		 * @param time time taken (in milliseconds)
		 */
		void printStats(const int64_t& length, const int64_t& time) const;


		NpuSocket& socket_;
		int64_t monitorStartTime_;
	};
}

#endif /* NPUSOCKETACTION_H_ */
