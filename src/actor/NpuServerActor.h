#ifndef NPUSERVER_H_
#define NPUSERVER_H_

#include "NpuActor.h"

namespace npu {

	/**
	 * Listens on a UDP socket for incoming client connections,
	 * spawns a new thread for each, and handles any client requests.
	 */
	class NpuServerActor : public NpuActor {
	public:
		NpuServerActor(const NpuConfig& config);

		virtual ~NpuServerActor();

		/**
		 * Listens for NPU client connections.
		 *
		 * @return error code, 0 for success
		 */
		virtual int execute();

		virtual const std::string toString() const;
	};
}

#endif /* NPUSERVER_H_ */
