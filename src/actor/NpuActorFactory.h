#ifndef NPUACTORBUILDER_H_
#define NPUACTORBUILDER_H_

#include "NpuIntent.h"

namespace npu {
	class NpuConfig;
	class NpuActor;

	class NpuActorFactory {
	public:
		NpuActorFactory();

		~NpuActorFactory();

		/**
		 * Builds an actor based upon the specified configuration.
		 *
		 * @param config configuration to use
		 * @param action action to be performed
		 * @return actor, or NULL
		 */
		NpuActor* const build(const NpuConfig& config, const NpuIntent& action);

		/**
		 * Builds an actor based upon the specified configuration,
		 * and then executes it.
		 *
		 * @param config configuration to use
		 * @param action action to be performed
		 * @return error code, or 0 for success
		 */
		int buildAndExecute(const NpuConfig& config, const NpuIntent& action);
	};
}

#endif /* NPUACTORBUILDER_H_ */
