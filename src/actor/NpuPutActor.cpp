#include "NpuPutActor.h"

#include "action/NpuPutAction.h"
#include "NpuConfig.h"

namespace npu {

	NpuPutActor::NpuPutActor(const NpuConfig& config) : NpuClientActor(config) {
	}

	NpuPutActor::~NpuPutActor() {
	}

	int NpuPutActor::doExecute(NpuSocket& socket) {
		NpuPutAction action(socket, config().sourcePath(), config().destinationPath());
		return action.execute();
	}

	const std::string NpuPutActor::toString() const {
		return "PutActor";
	}
}
