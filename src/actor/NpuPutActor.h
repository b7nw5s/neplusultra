#ifndef NPUPUTACTOR_H_
#define NPUPUTACTOR_H_

#include "NpuClientActor.h"

namespace npu {

	/**
	 * Connects to an NPU server and performs a PUT action.
	 */
	class NpuPutActor : public NpuClientActor {
	public:
		NpuPutActor(const NpuConfig& config);

		~NpuPutActor();


		const std::string toString() const;


	protected:
		/**
		 * Performs the actual client action.
		 *
		 * @param socket socket
		 * @return error code, or 0 for success
		 */
		int doExecute(NpuSocket& socket);
	};
}

#endif /* NPUPUTACTOR_H_ */
