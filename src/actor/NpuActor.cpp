#include "NpuActor.h"

#include <udt4/udt.h>

#include "NpuConfig.h"

namespace npu {

	NpuActor::NpuActor(const NpuConfig& config) : config_(config) {
		UDT::startup();
	}

	NpuActor::~NpuActor() {
		UDT::cleanup();
	}

	const NpuConfig& NpuActor::config() const {
		return config_;
	}
}
