#ifndef NPUCLIENT_H_
#define NPUCLIENT_H_

#include "NpuActor.h"

namespace npu {
	class NpuSocket;

	/**
	 * Connects to an NPU server and performs an action.
	 */
	class NpuClientActor : public NpuActor {
	public:
		virtual ~NpuClientActor();

		/**
		 * Connects to the NPU server.
		 *
		 * @return error code, 0 for success
		 */
		virtual int execute();

		virtual const std::string toString() const;


	protected:
		NpuClientActor(const NpuConfig& config);

		/**
		 * Performs the actual client action.
		 *
		 * @param socket socket
		 * @return error code, or 0 for success
		 */
		virtual int doExecute(NpuSocket& socket) = 0;
	};
}

#endif /* NPUCLIENT_H_ */
