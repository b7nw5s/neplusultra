#include "NpuClientActor.h"

#include "util/DetectOS.h"
#if OS_POSIX_X11 || OS_APPLE_OSX
	#include <unistd.h>
	#include <cstdlib>
	#include <cstring>
	#include <netdb.h>
#else
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <wspiapi.h>
#endif

#include <memory>
#include <string>
#include <iostream>
#include <sstream>
#include <udt4/udt.h>

#include "socket/NpuSocket.h"
#include "NpuConfig.h"
#include "NpuErrorCode.h"

namespace npu {

	NpuClientActor::NpuClientActor(const NpuConfig& config) : NpuActor(config) {
	}

	NpuClientActor::~NpuClientActor() {
	}

	int NpuClientActor::execute() {
		struct addrinfo hints, *local, *remote;
		memset(&hints, 0, sizeof(struct addrinfo));

		hints.ai_flags = AI_PASSIVE;
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;

		std::stringstream localService;
		localService << config::DEFAULT_PORT;
		if (0 != getaddrinfo(NULL, localService.str().c_str(), &hints, &local)) {
			std::cout << "invalid local network address" << std::endl;
			return ERR_INVALID_LOCAL_SOCKET;
		}
		UDTSOCKET udtClientSocket = UDT::socket(local->ai_family, local->ai_socktype, local->ai_protocol);
		// UDT Options
		//UDT::setsockopt(udtClientSocket, 0, UDT_CC, new CCCFactory<CUDPBlast>(), sizeof(CCCFactory<CUDPBlast>));
		//UDT::setsockopt(udtClientSocket, 0, UDT_MSS, new int(9000), sizeof(int));
		//UDT::setsockopt(udtClientSocket, 0, UDT_SNDBUF, new int(10000000), sizeof(int));
		//UDT::setsockopt(udtClientSocket, 0, UDP_SNDBUF, new int(10000000), sizeof(int));
		//UDT::setsockopt(udtClientSocket, 0, UDT_MAXBW, new int64_t(12500000), sizeof(int));
		freeaddrinfo(local);

		// Windows UDP issue
		// For better performance, modify HKLM\System\CurrentControlSet\Services\Afd\Parameters\FastSendDatagramThreshold
		#if OS_MS_WINDOWS
		UDT::setsockopt(udtClientSocket, 0, UDT_MSS, new int(1052), sizeof(int));
		#endif

		std::stringstream remoteService;
		remoteService << config().port();
		if (0 != getaddrinfo(config().remoteHost().c_str(), remoteService.str().c_str(), &hints, &remote)) {
			std::cout << "invalid server address: " << config().remoteHost() << ":" << config().port() << std::endl;
			return ERR_INVALID_REMOTE_SOCKET;
		}
		// Connect to the server, with implicit bind.
		if (UDT::ERROR == UDT::connect(udtClientSocket, remote->ai_addr, remote->ai_addrlen)) {
			freeaddrinfo(remote);
			std::cout << "socket connect failed: " << UDT::getlasterror().getErrorMessage() << std::endl;
			return ERR_CONNECT_FAILED;
		}
		freeaddrinfo(remote);

		std::unique_ptr<NpuSocket> npuSocket(new NpuSocket(udtClientSocket));
		doExecute(*npuSocket);

		// Close the socket.
		if (UDT::getsockstate(udtClientSocket) == CONNECTED) {
			npuSocket->sendCommand(CMD_CLOSE);
		}
		UDT::close(udtClientSocket);
		return SUCCESS;
	}

	const std::string NpuClientActor::toString() const {
		return "ClientActor";
	}
}
