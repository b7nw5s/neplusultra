#ifndef NPUGETACTOR_H_
#define NPUGETACTOR_H_

#include "NpuClientActor.h"

namespace npu {

	/**
	 * Connects to an NPU server and performs a GET action.
	 */
	class NpuGetActor : public NpuClientActor {
	public:
		NpuGetActor(const NpuConfig& config);

		~NpuGetActor();


		const std::string toString() const;


	protected:
		/**
		 * Performs the actual client action.
		 *
		 * @param socket socket
		 * @return error code, or 0 for success
		 */
		int doExecute(NpuSocket& socket);
	};
}

#endif /* NPUGETACTOR_H_ */
