#include "NpuServerActor.h"

#include "util/DetectOS.h"
#if OS_POSIX_X11 || OS_APPLE_OSX
	#include <unistd.h>
	#include <cstdlib>
	#include <cstring>
	#include <netdb.h>
#else
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <wspiapi.h>
#endif

#include <memory>
#include <string>
#include <iostream>
#include <sstream>
#include <udt4/udt.h>

#include "util/Runnable.h"

#include "socket/NpuSocket.h"
#include "action/NpuListenAction.h"
#include "NpuConfig.h"
#include "NpuErrorCode.h"

namespace npu {

	NpuServerActor::NpuServerActor(const NpuConfig& config) : NpuActor(config) {
	}

	NpuServerActor::~NpuServerActor() {
	}

	RUNNABLE(receive) {
		std::unique_ptr<NpuSocket> npuSocket(static_cast<NpuSocket*>(argv));
		if (npuSocket.get()) {
			NpuListenAction action(*npuSocket);
			action.execute();
		}
		RUNNABLE_RETURN();
	}

	int NpuServerActor::execute() {
		struct addrinfo hints, *local;
		memset(&hints, 0, sizeof(struct addrinfo));

		hints.ai_flags = AI_PASSIVE;
		hints.ai_family = AF_INET;
		hints.ai_socktype = SOCK_STREAM;

		std::stringstream service;
		service << config().port();
		if (0 != getaddrinfo(NULL, service.str().c_str(), &hints, &local)) {
			std::cout << "port " << config().port() << " already in use" << std::endl;
			return ERR_INVALID_LOCAL_SOCKET;
		}
		UDTSOCKET udtListenSocket = UDT::socket(local->ai_family, local->ai_socktype, local->ai_protocol);
		// UDT Options
		//UDT::setsockopt(udtListenSocket, 0, UDT_CC, new CCCFactory<CUDPBlast>(), sizeof(CCCFactory<CUDPBlast>));
		//UDT::setsockopt(udtListenSocket, 0, UDT_MSS, new int(9000), sizeof(int));
		//UDT::setsockopt(udtListenSocket, 0, UDT_RCVBUF, new int(10000000), sizeof(int));
		//UDT::setsockopt(udtListenSocket, 0, UDP_RCVBUF, new int(10000000), sizeof(int));

		if (UDT::ERROR == UDT::bind(udtListenSocket, local->ai_addr, local->ai_addrlen)) {
			freeaddrinfo(local);
			std::cout << "socket bind failed: " << UDT::getlasterror().getErrorMessage() << std::endl;
			return ERR_SOCKET_BIND_FAILED;
		}
		freeaddrinfo(local);

		static const int MAX_PENDING_CONNECTIONS = 10;
		if (UDT::ERROR == UDT::listen(udtListenSocket, MAX_PENDING_CONNECTIONS)) {
			std::cout << "socket listen failed: " << UDT::getlasterror().getErrorMessage() << std::endl;
			return ERR_SOCKET_LISTEN_FAILED;
		}
		std::cout << "server listening on port: " << config().port() << std::endl;

		while (true) {
			sockaddr_storage clientaddr;
			int addrlen = sizeof(clientaddr);
			UDTSOCKET udtClientSocket;

			if (UDT::INVALID_SOCK == (udtClientSocket = UDT::accept(udtListenSocket, reinterpret_cast<sockaddr*>(&clientaddr), &addrlen))) {
				std::cout << "socket accept failed: " << UDT::getlasterror().getErrorMessage() << std::endl;
				continue;
			}
			char clientHost[NI_MAXHOST];
			char clientService[NI_MAXSERV];
			getnameinfo(reinterpret_cast<sockaddr*>(&clientaddr), addrlen, clientHost, sizeof(clientHost), clientService, sizeof(clientService), NI_NUMERICHOST|NI_NUMERICSERV);
			std::cout << "connected: " << clientHost << ":" << clientService << std::endl;

			THREAD receiverThread;
			FORK_THREAD(receiverThread, receive, new NpuSocket(udtClientSocket));
		}

		UDT::close(udtListenSocket);
		return SUCCESS;
	}

	const std::string NpuServerActor::toString() const {
		return "ServerActor";
	}
}
