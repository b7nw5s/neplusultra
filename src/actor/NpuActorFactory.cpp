#include "NpuActorFactory.h"

#include <memory>

#include "actor/NpuServerActor.h"
#include "actor/NpuGetActor.h"
#include "actor/NpuPutActor.h"
#include "NpuConfig.h"
#include "NpuErrorCode.h"

namespace npu {

	NpuActorFactory::NpuActorFactory() {
	}

	NpuActorFactory::~NpuActorFactory() {
	}

	NpuActor* const NpuActorFactory::build(const NpuConfig& config, const NpuIntent& action) {
		switch (action) {
		case INTENT_LISTEN:
			return new NpuServerActor(config);
		case INTENT_GET:
			return new NpuGetActor(config);
		case INTENT_PUT:
			return new NpuPutActor(config);
		default:
			return 0;
		}
	}

	int NpuActorFactory::buildAndExecute(const NpuConfig& config, const NpuIntent& action) {
		int result = SUCCESS;
		std::unique_ptr<NpuActor> actor(build(config, action));
		if (actor.get()) {
			result = actor->execute();
		}
		return result;
	}
}
