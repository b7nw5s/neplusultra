#ifndef NPUACTOR_H_
#define NPUACTOR_H_

#include <string>

namespace npu {
	class NpuConfig;

	class NpuActor {
	public:
		virtual ~NpuActor();

		/**
		 * Executes the action.
		 *
		 * @return error code, or 0 for success
		 */
		virtual int execute() = 0;

		/**
		 * Gets the type of the actor.
		 *
		 * @return type
		 */
		virtual const std::string toString() const = 0;


	protected:
		NpuActor(const NpuConfig& config);

		const NpuConfig& config() const;

	private:
		const NpuConfig& config_;
	};
}

#endif /* ACTOR_NPUACTOR_H_ */
