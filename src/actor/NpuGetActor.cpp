#include "NpuGetActor.h"

#include "action/NpuGetAction.h"
#include "NpuConfig.h"

namespace npu {

	NpuGetActor::NpuGetActor(const NpuConfig& config) : NpuClientActor(config) {
	}

	NpuGetActor::~NpuGetActor() {
	}

	int NpuGetActor::doExecute(NpuSocket& socket) {
		NpuGetAction action(socket, config().sourcePath(), config().destinationPath());
		return action.execute();
	}

	const std::string NpuGetActor::toString() const {
		return "GetActor";
	}
}
