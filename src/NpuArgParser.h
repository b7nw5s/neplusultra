#ifndef NPUARGPARSER_H_
#define NPUARGPARSER_H_

#include <string>

#include "NpuIntent.h"

namespace npu {
	class NpuConfig;

	class NpuArgParser {
	public:
		NpuArgParser();

		~NpuArgParser();

		/**
		 * Parses the set of command line arguments and populates an NpuConfig instance.
		 *
		 * @param argc argument count
		 * @param argv argument values
		 * @param config configuration (output)
		 * @param intent intent (output)
		 * @return error code, 0 for success
		 */
		int parse(int argc, char* argv[], NpuConfig& config, NpuIntent& intent) const;

	private:
		/**
		 * Parse configuration source and destination strings
		 * to determine the remote host, command to execute,
		 * and the source and destination file paths.
		 *
		 * @param source source
		 * @param destination destination
		 * @param config config (output)
		 * @param intent intent (output)
		 */
		void parseSourceAndDestination(
				const std::string& source, const std::string& destination,
				NpuConfig& config, NpuIntent& intent) const;

		void splitOnColon(const std::string& input,
					std::string& before, std::string& after) const;
	};
}

#endif /* NPUARGPARSER_H_ */
