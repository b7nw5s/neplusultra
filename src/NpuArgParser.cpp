#include "NpuArgParser.h"

#include <memory>
#include <iostream>
#include <sstream>

#include "util/optionparser.h"

#include "NpuVersion.h"
#include "NpuConfig.h"
#include "NpuErrorCode.h"

namespace npu {

	NpuArgParser::NpuArgParser() {
	}

	NpuArgParser::~NpuArgParser() {
	}

	int NpuArgParser::parse(int argc, char* argv[], NpuConfig& config, NpuIntent& intent) const {
		intent = INTENT_UNKNOWN;

		enum optionIndex { UNKNOWN, HELP, VERSION, LISTEN, INTERACTIVE, PORT };
		static const option::Descriptor usage[] = {
			{UNKNOWN, 0, "" , ""    ,            option::Arg::None,     "Usage: npu [OPTION]... SOURCE DESTINATION\n\n"
															   	   	    "Options:" },
			{HELP,    0, "h", "help",            option::Arg::None,     "  --help, -h  \tPrint usage and exit." },
			{VERSION, 0, "v", "version",         option::Arg::None,     "  --version, -v  \tPrint version and exit." },
			{LISTEN,  0, "l", "listen",          option::Arg::None,     "  --listen, -l  \tListen for connections." },
			{INTERACTIVE, 0, "i", "interactive", option::Arg::Optional, "  --interactive, -i  \tInterative mode."},
			{PORT,    0, "p", "port",            option::Arg::Optional, "  --port, -p  \tSet UDP port number."},
			{UNKNOWN, 0, "" ,  ""   ,            option::Arg::None,     "\nExamples:\n"
															            "  npu -l\n"
															   	   	    "  npu -l -p9000\n"
															   	   	    "  npu 10.0.0.1:/path/to/file.txt local/path/to/file.txt\n"
															   	   	    "  npu -p9000 local/path/to/file.txt 10.0.0.1:/path/to/file.txt\n" },
			{0,0,0,0,0,0}
		};

		// skip program name argv[0] if present
		argc -= (argc > 0);
		argv += (argc > 0);

		option::Stats stats(usage, argc, argv);
		std::unique_ptr<option::Option[]> options(new option::Option[stats.options_max]);
		std::unique_ptr<option::Option[]> buffer(new option::Option[stats.buffer_max]);
		option::Parser parse(usage, argc, argv, options.get(), buffer.get());

		if (parse.error()) {
			return ERR_INVALID_ARGUMENTS;
		}
		if (options[HELP] || argc == 0) {
			option::printUsage(std::cout, usage);
			return SUCCESS;
		}
		if (options[VERSION]) {
			std::cout << "Ne Plus Ultra " <<
					'v' << config::VERSION_MAJOR
					<< '.' << config::VERSION_MINOR
					<< '.' << config::VERSION_PATCH
					<< std::endl;
			return SUCCESS;
		}
		if (options[UNKNOWN]) {
			for (option::Option* opt = options[UNKNOWN]; opt; opt = opt->next()) {
				std::cout << "Unknown option: " << opt->name << "\n";
			}
			return ERR_INVALID_ARGUMENTS;
		}
		intent = options[LISTEN] ? INTENT_LISTEN : INTENT_UNKNOWN;
		const option::Option* const portOption = options[PORT];
		config.setPort(portOption && portOption->arg ? atoi(portOption->arg) : config::DEFAULT_PORT);

		std::string source, destination;
		for (int i = 0; i < parse.nonOptionsCount(); ++i) {
			const std::string value(parse.nonOption(i));
			switch (i) {
			case 0:
				source = value;
				break;
			case 1:
				destination = value;
				break;
			}
		}
		// If not listening (server mode) then must supply source and destination.
		if (intent != INTENT_LISTEN && (source.empty() || destination.empty())) {
			option::printUsage(std::cout, usage);
			return ERR_INVALID_ARGUMENTS;
		}
		if (!source.empty() && !destination.empty()) {
			parseSourceAndDestination(source, destination, config, intent);
		}
		return 0;
	}

	void NpuArgParser::parseSourceAndDestination(
			const std::string& source, const std::string& destination,
			NpuConfig& config, NpuIntent& intent) const {

		std::string before, after;
		splitOnColon(source, before, after);
		if (!after.empty()) {
			// Transfer from server to client (GET).
			intent = INTENT_GET;
			config.setRemoteHost(before);
			config.setSourcePath(after);
			config.setDestinationPath(destination);
			return;
		}

		splitOnColon(destination, before, after);
		if (!after.empty()) {
			// Transfer from client to server (PUT).
			intent = INTENT_PUT;
			config.setRemoteHost(before);
			config.setSourcePath(source);
			config.setDestinationPath(after);
			return;
		}

		intent = INTENT_UNKNOWN;
	}

	void NpuArgParser::splitOnColon(const std::string& input,
			std::string& before, std::string& after) const {
		const char delimiter = ':';
		std::stringstream ss;
		ss.str(input);
		std::getline(ss, before, delimiter);
		std::getline(ss, after, delimiter);
	}
}
