#ifndef FILESYSTEM_H_
#define FILESYSTEM_H_

#include <stdint.h>
#include <string>
#include <vector>

namespace util {
namespace fs {

	const char pathDelimiter();

	const bool hasPathDelimiter(const std::string& path);

	void splitBy(const std::string& input, const char delimiter, std::vector<std::string>& tokens);

	void splitPath(const std::string& path, std::vector<std::string>& elements);

	void appendSlash(std::string& path);

	const std::string basename(const std::string& path);

	const bool isDirectory(const std::string& path);

	const int list(const std::string& path, std::vector<std::string>& children);

	void makeDir(const std::string& path);

	void makeDirs(const std::string& path);

	const std::string appendBasename(const std::string& path, const std::string& basename);

	const std::string appendToPath(const std::string& path, const std::string& child);

	const int64_t fileLength(const std::string& path);
}}

#endif /* FILESYSTEM_H_ */
