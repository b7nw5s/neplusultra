#ifndef RUNNABLE_H_
#define RUNNABLE_H_

#include "DetectOS.h"

namespace npu {

	#if OS_POSIX_X11 || OS_APPLE_OSX
		#include <pthread.h>

		#define RUNNABLE(FUNCTION) void* FUNCTION(void* argv)
		#define RUNNABLE_RETURN() return 0

		#define THREAD pthread_t
		#define FORK_THREAD(THREAD, FUNCTION, ARGV) pthread_create(&THREAD, NULL, FUNCTION, ARGV); pthread_detach(THREAD)
	#else
		#define RUNNABLE(NAME) DWORD WINAPI NAME(LPVOID argv)
		#define RUNNABLE_RETURN() return NULL

		#define THREAD HANDLE
		#define FORK_THREAD(THREAD, FUNCTION, ARGV) THREAD = CreateThread(NULL, 0, FUNCTION, ARGV, 0, NULL)
	#endif
}

#endif /* RUNNABLE_H_ */
