#ifndef SLEEP_H_
#define SLEEP_H_

#include "DetectOS.h"

#if OS_POSIX_X11 || OS_APPLE_OSX
	#include <unistd.h>
	#define SLEEP(SECONDS) sleep(SECONDS)
#else
	#define SLEEP(SECONDS) Sleep(SECONDS * 1000)
#endif

#endif /* SLEEP_H_ */
