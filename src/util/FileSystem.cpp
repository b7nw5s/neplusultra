#include "FileSystem.h"

#include <sstream>
#include <fstream>

#include "DetectOS.h"
#if OS_POSIX_X11 || OS_APPLE_OSX
	#include <dirent.h>
	#include <sys/types.h>
	#include <sys/stat.h>
#elif OS_MS_WINDOWS
	#include "dirent.h";
	#include <direct.h>
#endif

namespace util {
namespace fs {

	const char pathDelimiter() {
		static const char PATH_DELIMITER = '/';
		return PATH_DELIMITER;
	}

	const bool hasPathDelimiter(const std::string& path) {
		return (path.length() > 0 && path.substr(path.length() - 1)[0] == pathDelimiter());
	}

	void splitBy(const std::string& input, const char delimiter, std::vector<std::string>& tokens) {
		std::stringstream iss;
		iss.str(input);
		std::string token;
		do {
			std::getline(iss, token, delimiter);
			tokens.push_back(token);
		} while (!iss.eof());
	}

	void splitPath(const std::string& path, std::vector<std::string>& elements) {
		splitBy(path, pathDelimiter(), elements);
	}

	void appendSlash(std::string& path) {
		if (!hasPathDelimiter(path)) {
			path += pathDelimiter();
		}
	}

	const std::string basename(const std::string& path) {
		std::vector<std::string> elements;
		splitPath(path, elements);
		if (!elements.empty() && elements.rbegin()->empty()) {
			elements.pop_back();
		}
		return (elements.empty() ? "" : *elements.rbegin());
	}

	const bool isDirectory(const std::string& path) {
		bool exists = false;
		DIR* const dir = opendir(path.c_str());
		if (dir != NULL) {
			exists = true;
			closedir(dir);
		}
		return exists;
	}

	const int list(const std::string& path, std::vector<std::string>& children) {
		DIR* dir = NULL;
		struct dirent* ent = NULL;
		if ((dir = opendir(path.c_str())) != NULL) {
			// Print all the files and directories within the directory.
			while ((ent = readdir(dir)) != NULL) {
				const std::string child(ent->d_name);
				// Filter out "." and ".." directory entries.
				if (child != "." && child != "..") {
					children.push_back(std::string(ent->d_name));
				}
			}
			closedir(dir);
			return children.size();
		}
		return 0;
	}

	void makeDir(const std::string& path) {
#if OS_POSIX_X11
		mkdir(path.c_str(), S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
#elif OS_MS_WINDOWS
		_mkdir(path.c_str());
#endif
	}

	void makeDirs(const std::string& path) {
		std::vector<std::string> elements;
		splitPath(path, elements);
		std::stringstream dirPath;
		if (elements.size() > 0) {
			for (unsigned int i = 0; i < (elements.size() - 1); ++i) {
				dirPath << elements[i];
				const std::string curDirPath(dirPath.str());
				if (!curDirPath.empty() && !isDirectory(curDirPath)) {
					makeDir(curDirPath);
				}
				dirPath << pathDelimiter();
			}
		}
	}

	const std::string appendBasename(const std::string& path, const std::string& basename) {
		if (util::fs::basename(path) != basename) {
			std::stringstream ssPathResult;
			ssPathResult << path;
			ssPathResult << util::fs::pathDelimiter() << basename;
			return ssPathResult.str();
		}
		return path;
	}

	const std::string appendToPath(const std::string& path, const std::string& child) {
		std::stringstream ssPathResult;
		ssPathResult << path;
		if (!util::fs::hasPathDelimiter(path)) {
			ssPathResult << util::fs::pathDelimiter();
		}
		ssPathResult << child;
		return ssPathResult.str();
	}

	const int64_t fileLength(const std::string& path) {
		// Check is not a directory.
		if (!util::fs::isDirectory(path)) {
			std::fstream fileInputStream(path, std::ios::in | std::ios::binary);
			if (fileInputStream.is_open()) {
				fileInputStream.seekg(0, std::ios::end);
				return static_cast<int64_t>(fileInputStream.tellg());
			}
		}
		return 0;
	}
}}
