#ifndef WINCLUDE_H_
#define WINCLUDE_H_

#include "DetectOS.h"

// Include this header file instead of directly 
// including <windows.h> to ensure that the 
// appropriate defines are always set first.

#if OS_MS_WINDOWS

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif

// Prevent conflict with min()/max() methods 
// of the std::numeric_limits<T> class.
#ifndef NOMINMAX
#define NOMINMAX 1
#endif

#include <windows.h>

#endif /* OS_MS_WINDOWS */

#endif /* WINCLUDE_H_ */
