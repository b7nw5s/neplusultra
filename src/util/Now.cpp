#include "Now.h"

// C++11
#include <chrono>

namespace util {
namespace time {

	const int64_t now() {
		const std::chrono::milliseconds msSinceEpoch = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()
		);
		return static_cast<int64_t>(msSinceEpoch.count());
	}
}}
