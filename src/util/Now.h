#ifndef NOW_H_
#define NOW_H_

#include <stdint.h>

namespace util {
namespace time {

	const int64_t now();
}}

#endif /* NOW_H_ */
