#ifndef DETECTOS_H_
#define DETECTOS_H_
	
// Macros to detect which type of Operating System we are compiling for.	
#if !defined(OS_POSIX_X11) && !defined(OS_MS_WINDOWS) && !defined(OS_APPLE_OSX) && !defined(OS_SOLARIS)
#if defined(_MSC_VER) || defined(__WATCOMC__) || defined(__MINGW32__) || \
	defined(_WIN32) || defined(_WIN64) || \
	(defined(__CYGWIN__) && defined(X_DISPLAY_MISSING))
#define OS_MS_WINDOWS 1

#elif defined(__posix__) || defined(__unix__) || defined(__linux__) || defined(__sun)
#define OS_POSIX_X11 1

#elif defined(__APPLE__)
#define OS_APPLE_OSX 1

#else
#error "Unsupported Operating System"

#endif
#endif

#if !defined(OS_POSIX_X11)
#define OS_POSIX_X11 0
#endif

#if !defined(OS_MS_WINDOWS)
#define OS_MS_WINDOWS 0
#endif

#if !defined(OS_APPLE_OSX)
#define OS_APPLE_OSX 0
#endif

#endif /* DETECTOS_H_ */
