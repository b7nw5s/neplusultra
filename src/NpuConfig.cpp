#include "NpuConfig.h"

namespace npu {

	NpuConfig::NpuConfig() : port_(config::DEFAULT_PORT) {
	}

	NpuConfig::NpuConfig(const std::string& remoteHost,
			const int port,
			const std::string& sourcePath,
			const std::string& destinationPath) :
					remoteHost_(remoteHost),
					sourcePath_(sourcePath),
					destinationPath_(destinationPath),
					port_(port) {
	}

	NpuConfig::NpuConfig(const NpuConfig& src) :
			remoteHost_(src.remoteHost_),
			sourcePath_(src.sourcePath_),
			destinationPath_(src.destinationPath_),
			port_(src.port_) {
	}

	NpuConfig::~NpuConfig() {
	}


	NpuConfig& NpuConfig::operator= (const NpuConfig& rhs) {
		remoteHost_ = rhs.remoteHost_;
		sourcePath_ = rhs.sourcePath_;
		destinationPath_ = rhs.destinationPath_;
		port_ = rhs.port_;
		return *this;
	}
}
