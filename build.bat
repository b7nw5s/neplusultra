@echo off
rem
rem Builds in either Debug or Release (default) mode.
rem

set "CRE_GENERATOR_TYPE=Visual Studio 10 Win64"

set "CRE_BUILD_TYPE=Release"
if "%~1"=="debug" ( set "CRE_BUILD_TYPE=Debug" )
if "%~1"=="Debug" ( set "CRE_BUILD_TYPE=Debug" )
if "%~1"=="release" ( set "CRE_BUILD_TYPE=Release" )
if "%~1"=="Release" ( set "CRE_BUILD_TYPE=Release" )
echo -- Build type is: %CRE_BUILD_TYPE%
echo -- Generator type is: %CRE_GENERATOR_TYPE%

rem Create build directory if it does not exist.
if not exist "build" (
  mkdir "build"
)
if not exist "build/%CRE_BUILD_TYPE%" (
  mkdir "build/%CRE_BUILD_TYPE%"
)
rem Change to build directory.
if exist "build/%CRE_BUILD_TYPE%" (
  cd "build/%CRE_BUILD_TYPE%"
) else (
  echo Build directory build/%CRE_BUILD_TYPE% does not exist.
  exit
)

rem Generate arguments to pass to cmake.
set "CRE_CMAKE_ARGS=-DCMAKE_BUILD_TYPE=%CRE_BUILD_TYPE% -G ^"%CRE_GENERATOR_TYPE%^""
rem Call cmake to generate the build files.
cmake %CRE_CMAKE_ARGS% ../../

rem Call msbuild passing through the remaining command line arguments.
rem msbuild $@

rem Return to original directory containing the build.bat file.
cd ../../
